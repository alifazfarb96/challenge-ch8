
const Table = ({
    player = [],
    onChange
}) => {

    return (
        <div>
            <table className="Table">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Experience</th>
                        <th>Level</th>
                    </tr>
                </thead>
                <tbody>
                    {player.map((data, index) => (
                        <tr key={index}>
                            <td>{data.username}</td>
                            <td>{data.email}</td>
                            <td>{data.password}</td>
                            <td>{data.experience}</td>
                            <td>{data.lvl}</td>
                        </tr>
                    ))}
                    </tbody>
            </table>
            
        </div>
    )
}

export default Table
