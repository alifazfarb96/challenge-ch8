import './App.css';
import React, { useState, useEffect } from 'react';
import Table from './Table';

function App() {

  useEffect(
      () => {
        setPlayer([
          {
            username: "John Doe",
            email: "johndoe@email.com",
            password: "12345678",
            experience: 1000,
            lvl: 1
          },
          {
            username: "testplayer1",
            email: "testplayer1@email.com",
            password: "test",
            experience: 5000,
            lvl: 5,
          },
          {
            username: "testplayer2",
            email: "testplayer2@email.com",
            password: "test",
            experience: 5000,
            lvl: 5,
          }
        ])
      
      },
      []
  )
  
  var [username, setUsername] = useState("")
  var [email, setEmail] = useState("")
  var [password, setPassword] = useState("")
  var [experience, setExperience] = useState("")
  var [lvl, setLvl] = useState("")

  const [getUsername, setGetUsername] = useState("")
  const [newUsername, setNewUsername] = useState("")
  const [newPassword, setNewPassword] = useState("")

  const [searchUsername, setSearchUsername] = useState("")
  const [searchEmail, setSearchEmail] = useState("")
  const [searchExperience, setSearchExperience] = useState("")
  const [searchLvl, setSearchLvl] = useState("")

  const [player, setPlayer] = useState([])

  const handleAddLog = (listPlayers) => {
    setPlayer([...player,listPlayers])
  }

  const handleRegister = () => {
    if (username && email && password) {
      experience = 0;
      lvl = 0;
      const listPlayers = {username, email, password, experience, lvl}
      handleAddLog(listPlayers)
    }
  }

  const handleEdit = () => {
    const index = player.map(e => e.username).indexOf(getUsername);
    if (index !== -1) {
      console.log(index)
      const playerEdit = player[index]
      console.log(playerEdit)
      username = newUsername
      if (newUsername) {
        username = newUsername
      } else { username = playerEdit.username}
      if (newPassword) {
        password = newPassword
      } else { password = playerEdit.password}
      email = playerEdit.email
      experience = playerEdit.experience
      lvl = playerEdit.lvl
      const listPlayers = { username, email, password, experience, lvl }
      handleAddLog(listPlayers)
    }
  }

  const handleSearch = () => {
    let conditions = [];
    if (searchUsername) {
      conditions.push({ key: "username", value: searchUsername});
    }
    if (searchEmail) {
      conditions.push({ key: "email", value: searchEmail});
    }
    if (searchExperience) {
      conditions.push({ key: "experience", value: searchExperience });
    }
    if (searchLvl) {
      conditions.push({ key: "lvl", value: searchLvl });
    }

    var filter = {};
    for (var i = 0; i < conditions.length; i++) {
      filter[conditions[i].key] = conditions[i].value;
    }

    let results = player.filter(function(item) {
      for (var key in filter) {
        if (item[key] === undefined || item[key] !== filter[key])
          return false;
      }
      return true;
    });

    for (var j = 0; j < results.length; j++) {
      console.log(results[j])
      handleAddLog(results[j]);
    }
    
  }

  return (
    <div className="App">
      <div className="Panel">

        <div className="Panel">
          <h2 className='Menu-text'>Create Player</h2>
          <div className="form">
            <p>Username</p>
            <input name="username" placeholder="username" onChange={(e) => setUsername(e.target.value)} />
            <p>Email</p>
            <input name="email" placeholder="email" onChange={(e) => setEmail(e.target.value)} />
            <p>Password</p>
            <input name="password" placeholder="password" onChange={(e) => setPassword(e.target.value)} />
            <br></br>
            <button onClick={handleRegister}>Register</button>
          </div>  
        </div>

        <hr></hr>

        <div className="Panel">
          <h2 className='Menu-text'>Edit Player</h2>
          <div className="form">
            <p>Username</p>
            <input name="username" placeholder="username" onChange={(e) => setGetUsername(e.target.value)} />
            <p>New Username</p>
            <input name="newUsername" placeholder="new username" onChange={(e) => setNewUsername(e.target.value)} />
            <p>New Password</p>
            <input name="newPassword" placeholder="new password" onChange={(e) => setNewPassword(e.target.value)} />
            <br></br>
            <button onClick={handleEdit}>Apply</button>
          </div>
        </div>

        <hr></hr>

        <div className="Panel">
          <h2 className='Menu-text'>Find Player</h2>
          <div className="form">
            <p>Username</p>
            <input name="username" placeholder="username" onChange={(e) => setSearchUsername(e.target.value)} />
            <p>Email</p>
            <input name="email" placeholder="email" onChange={(e) => setSearchEmail(e.target.value)} />
            <p>Experience</p>
            <input name="experience" placeholder="experience" onChange={(e) => setSearchExperience(parseInt(e.target.value))} />
            <p>Level</p>
            <input name="lvl" placeholder="level" onChange={(e) => setSearchLvl(parseInt(e.target.value))} />
            <br></br>
            <button onClick={handleSearch}>Search</button>
          </div>
        </div>
      </div>

      <div className="Panel">
      <h2 className='Menu-text'>Table Log</h2>
        <Table player={player}  onChange={handleAddLog}/>
      </div>
    </div>
    
  );
}

export default App;
